# Sorting Visualizer

I started developing this application to learn the [AngularJS][an] and
[Bootstrap][bs] frameworks for my new job. Things turned ambitious at some point,
and I started adding new stuff left and right.

This is an app that provides a visualization of different sorting algorithms. It
is completely implemented using web technologies, and can be viewed in a web
browser.

It should only be run through a web server, because of how Angular works.

Everything is available under the [Apache License, Version 2.0][al].

[al]: http://www.apache.org/licenses/LICENSE-2.0
[an]: http://angularjs.org/
[bs]: http://twitter.github.io/bootstrap/
