//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// Sorting/app/controllers/SortingController.js
//
// Defines the controller that handles the sorting.
//------------------------------------------------------------------------------

app.controller('SortingController', function ($scope, $location, $log, PluginService) {

    //
    // Handle messages from worker
    //

    function messageHandler (e) {
        var data = e.data;

        switch (data.cmd) {
            case 'swap':
                swap(data.idx1, data.idx2);
                break;
            case 'updateValue':
                updateValue(data.idx, data.value);
                break;
            case 'done':
                done();
                break;
            case 'log':
                $log.log(data.message);
                break;
            default:
                $log.log('Unknown message: ' + e);
        }
    }

    //
    // Scope data object
    //

    $scope.data = {};
    $scope.private = {}; // NOTE not to be used outside of controller!

    //
    // Scope functions
    //

    $scope.gotoPluginSettings = function () {
        $location.path('/plugins');
    };

    $scope.sort = function () {
        if ($scope.private.sorting) {
            stop();
        } else {
            start();
        }
    };

    $scope.reset = function () {
        stop();
        $scope.data.values = randomize($scope.private.numbers);
        $scope.private.worker.postMessage({
            cmd: 'setValues',
            values: $scope.data.values
        });
    };

    $scope.currentPlugin = function () {
        return PluginService.selectedPlugin();
    };

    $scope.plugins = function () {
        return PluginService.plugins();
    };

    $scope.pluginName = function (plugin) {
        return PluginService.pluginName(plugin);
    };

    $scope.selectPlugin = function (plugin) {
        PluginService.setSelectedPlugin(plugin);
        $scope.private.worker.postMessage({
            cmd: 'load',
            pluginPath: 'app/plugins/' + PluginService.selectedPlugin() + '.js',
            plugin: PluginService.selectedPlugin()
        });
    };

    //
    // Utitily functions
    //

    function start() {
        $scope.private.worker.postMessage({ 'cmd': 'start'});
        $scope.private.sorting = true;
        $scope.data.sortButtonValue = 'Stop';
    }

    function stop() {
        $scope.private.worker.postMessage({ 'cmd': 'stop'});
        $scope.private.sorting = false;
        $scope.data.sortButtonValue = 'Start';
    }

    function done() {
        $scope.$apply(function () {
            $scope.private.sorting = false;
            $scope.data.sortButtonValue = 'Start';
        });
    }

    function swap (idx1, idx2) {
        $scope.$apply(function () {
            var tmp = $scope.data.values[idx1];
            $scope.data.values[idx1] = $scope.data.values[idx2];
            $scope.data.values[idx2] = tmp;
        });
    }

    function updateValue (idx, value) {
        $scope.$apply($scope.data.values[idx] = value);
    }

    function randomize(numbers) {
        var v = [];
        for (var i = 0; i < numbers; i++)
            v.push(Math.floor(Math.random() * 99) + 1);

        return v;
    }

    //
    // Initialize
    //

    function init () {

        //
        // Private data
        //

        $scope.private.worker = new Worker ('app/plugins/PluginRunner.js');
        $scope.private.sorting = false;
        $scope.private.numbers = 100;

        //
        // Initialize data values
        //

        $scope.data.values = randomize($scope.private.numbers);
        $scope.data.sortButtonValue = 'Start';

        // make sure we terminate the worker <-- TODO causes worker to be destroyed too soon??????
        // $scope.$on('$destroy', $scope.private.worker.terminate());

        //
        // Setup handler and initialize worker
        //

        $scope.private.worker.addEventListener('message', messageHandler, false);
        $scope.private.worker.postMessage({
            cmd: 'setValues',
            values: $scope.data.values
        });
        $scope.private.worker.postMessage({
            cmd: 'load',
            plugin: PluginService.selectedPlugin()
        });
    }

    init();

});
