//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// Sorting/app/controllers/PluginController.js
//
// Defines the controller that handles viewing and editing plugins for the
// sorting visualizer.
//------------------------------------------------------------------------------

app.controller('PluginController', function ($scope, $log, PluginService) {

    //
    // Scope functions
    //

    $scope.plugins = function () {
        return PluginService.plugins();
    };

    $scope.selectedPlugin = function () {
        return $scope.private.plugin;
    };

    $scope.selectPlugin = function (plugin) {
        $scope.private.plugin = plugin;
        load();
    };

    $scope.pluginName = function (plugin) {
        return PluginService.pluginName(plugin);
    };

    $scope.isPluginSelected = function (plugin) {
        return (plugin == $scope.private.plugin);
    };

    $scope.isPluginReadOnly = function (plugin) {
        return PluginService.isPluginBuiltin(plugin);
    };

    $scope.isPluginEditingDisabled = function (plugin) {
        return PluginService.pluginData(plugin).builtin;
    };

    $scope.pluginCode = function (plugin) {
        return PluginService.pluginData(plugin).code;
    };

    $scope.currentPlugin = {
        name: '',
        code: '',
        readOnly: false
    };

    $scope.documentation = {
        languageCollapsed: true,
        languageClass: function () {
            if (this.languageCollapsed) {
                return "icon-chevron-right";
            } else {
                return "icon-chevron-down";
            }
        },

        functionsCollapsed: true,
        functionsClass: function () {
            if (this.functionsCollapsed) {
                return "icon-chevron-right";
            } else {
                return "icon-chevron-down";
            }
        }
    };

    //
    // Local functions
    //

    function load() {
        var data = PluginService.pluginData($scope.private.plugin);

        $scope.currentPlugin.name = data.name;
        $scope.currentPlugin.code = data.code;
        $scope.currentPlugin.readOnly = data.builtin;
    }

    //
    // Initialize
    //

    function init () {
        $scope.private = {
            plugin: PluginService.selectedPlugin()
        };
        load();
    }

    init();
});
