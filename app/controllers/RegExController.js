//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

app.controller('RegExController', ['$scope', function ($scope) {
    $scope.testing = {
        input: '',
        output: ''
    };

    $scope.editorOptions = {
        theme: 'xq-light',
        lineNumbers: true,
        mode: 'javascript',
        autoFocus: true,
        height: 'dynamic',
        minHeight: '300'
    };

    var setOutput = function (output) {
        $scope.testing.output = output;
    }

    $scope.showOutput = function() {
        return $scope.testing.output === '';
    }

    $scope.$watch('testing.input', function() {
        setOutput('');
    });

    $scope.runParser = function () {
        try {
            var t = jslite.parse($scope.testing.input);
            setOutput(t);
        } catch (e) {
            setOutput(e.message);
        }
    }
}]);
