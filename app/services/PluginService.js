//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// Sorting/app/services/PluginService.js
//
// Knows of all the plugins installed, and allow the controllers to query
// information abou them.
//------------------------------------------------------------------------------

//
// Sorting namespace
//

var Sorting = {};

//
// PluginService
//

app.service('PluginService', function ($log) {
    this._plugins = {
        insertionsort: new Sorting.insertionsort(),
        bubblesort: new Sorting.bubblesort(),
        cocktailsort: new Sorting.cocktailsort(),
        quicksort: new Sorting.quicksort(),
    };

    this._selected = 'insertionsort';

    this.plugins = function () {
        var keys = [];
        for (var key in this._plugins) {
            keys.push(key);
        }
        return keys;
    };

    this.pluginName = function (plugin) {
        if (plugin in this._plugins) {
            if ('name' in this._plugins[plugin])
                return this._plugins[plugin].name;
        }

        return plugin;
    };

    this.isPluginBuiltin = function (plugin) {
        if (plugin in this._plugins) {
            if ('builtin' in this._plugins[plugin])
                return this._plugins[plugin].builtin;
        }

        return plugin;
    }

    this.setSelectedPlugin = function (plugin)
    {
        if (plugin in this._plugins) {
            this._selected = plugin;
        }
    };

    this.selectedPlugin = function () {
        return this._selected;
    };

    this.pluginData = function (plugin) {
        if (plugin in this._plugins) {
            return this._plugins[plugin];
        }

       return false;
    }
});
