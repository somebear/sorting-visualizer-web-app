//------------------------------------------------------------------------------
// Sorting/app/directives/sortingView.js
//
// Defines a directive that puts a google column chart into the specified tag.
//------------------------------------------------------------------------------

app.directive('jrVisualizer', function() {
    return {
        restrict: 'A',

        require: '?ngModel',

        scope: {
            model: '=ngModel'
        },

        link: function(scope, element, attr, controller) {
            if (!controller)
                return;

            var drawChart = function() {
                var options = {
                    backgroundColor: 'white',
                    bar: { groupWidth: '80%' },
                    chartArea: { top: 0, left: 0, width: '100%', height: '100%' },
                    enableInteractivity: false,
                    legend: { position: 'none' },
                    hAxis: { baseline: 0, baselineColor: 'white', textPosition: 'none', gridlines: { count: 0 } },
                    series: [{ color: 'red' }],
                    titlePosition: 'none',
                    vAxis: { baseline: 0, baselineColor: 'white', textPosition: 'none', gridlines: { count: 0 } }
                };

                var data = new google.visualization.DataTable();

                // add simple columns
                data.addColumn('string');
                data.addColumn('number');

                // set model
                angular.forEach (scope.model, function (value) {
                    data.addRow([value.toString(), value]);
                });

                // Instantiate and draw our chart, passing in some options.
                var columns = new google.visualization.ColumnChart(element[0]);
                columns.draw(data, options);
            };

            controller.$render = function() {
                drawChart();
            };

            scope.$watch('model', function () {
                controller.$render();
            }, true);
        }
    };
});
