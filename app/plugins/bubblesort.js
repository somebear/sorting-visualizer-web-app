//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// Sorting/app/plugins/bubblesort.js
//
// Performs bubble sort on the provided data set.
//------------------------------------------------------------------------------

Sorting.bubblesort = function() {

    this.name = 'Bubble Sort';

    this.builtin = true;

    this.code = "sort() {\n\
    n = length();\n\
    while (n > 0) {\n\
        newn = 0;\n\
        for (var i = 1; i < n; i++) {\n\
            if (value(i - 1) > value(i)) {\n\
                swap(i - 1, i);\n\
                newn = i;\n\
            }\n\
        }\n\
        n = newn;\n\
    }\n\
}\n\
";

    this.functions = {
        sort: {
            statements: [
                function (data) {
                    data.cs.n = data.length();
                },
                {
                    condition: function (data) {
                        return (data.cs.n > 0);
                    },
                    statements: [
                        function (data) {
                            data.cs.newn = 0;
                        },
                        function (data) {
                            data.cs.i = 1;
                        },
                        {
                            condition: function (data) {
                                return (data.cs.i < data.cs.n);
                            },
                            statements: [
                                {
                                    condition: function (data) {
                                        return (data.value(data.cs.i - 1) > data.value(data.cs.i));
                                    },
                                    then: [
                                        function (data) {
                                            data.swap(data.cs.i - 1, data.cs.i);
                                        },
                                        function (data) {
                                            data.cs.newn = data.cs.i;
                                        }
                                    ]
                                },
                                function (data) {
                                    data.cs.i++;
                                }
                            ]
                        },
                        function (data) {
                            data.cs.n = data.cs.newn;
                        }
                    ]
                }
            ]
        }
    };
};
