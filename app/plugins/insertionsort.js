//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// Sorting/app/plugins/insertionsort.js
//
// Performs insertion sort on the provided data set.
//------------------------------------------------------------------------------

Sorting.insertionsort = function() {

    this.name = 'Insertion Sort';

    this.builtin = true;

    this.code = "sort() {\n\
    for (var i = 1; i < length(); i++) {\n\
        var valueToInsert = value(i);\n\
        var holePos = i;\n\
\n\
        while (holePos > 0 && valueToInsert < value(holePos - 1)) {\n\
            updateValue(holePos, value(holePos - 1));\n\
            holePos--;\n\
        }\n\
\n\
        updateValue(holePos, valueToInsert);\n\
    }\n\
}\n\
";

    this.functions = {
        sort: {
            statements: [
                function (data) {
                    data.cs.i = 1;
                },
                {
                    condition: function (data) {
                        return (data.cs.i < data.length());
                    },
                    statements: [
                        function (data) {
                            data.cs.valueToInsert = data.value(data.cs.i);
                        },
                        function (data) {
                            data.cs.holePos = data.cs.i;
                        },
                        {
                            condition: function (data) {
                                return (data.cs.holePos > 0 &&
                                    (data.cs.valueToInsert <
                                        data.value(data.cs.holePos - 1)));
                            },
                            statements: [
                                function (data) {
                                    data.updateValue(
                                        data.cs.holePos,
                                        data.value(data.cs.holePos - 1));
                                },
                                function (data) {
                                    data.cs.holePos--;
                                }
                            ]
                        },
                        function (data) {
                            data.updateValue(
                                data.cs.holePos,
                                data.cs.valueToInsert);
                        },
                        function (data) {
                            data.cs.i++;
                        }
                    ]
                }
            ]
        }
    };
};
