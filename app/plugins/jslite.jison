//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Jison specification for a reduced version of JavaScript
// Adapted from http://www-archive.mozilla.org/js/language/grammar14.html
//------------------------------------------------------------------------------

%lex
integer                     "-"?(?:[0-9]|[1-9][0-9]+)
identifier                  [_$a-zA-Z][_$a-zA-Z0-9]*
esc                         "\\"
multiline                   \/\*([\s\S]*?)\*\/
singleline                  \/\/[^\r\n]*
dstring                      \"(?:{esc}["bfnrt/{esc}]|[^"{esc}])*\"
sstring                      \'(?:{esc}['bfnrt/{esc}]|[^'{esc}])*\'

%%
{singleline}                /* skip singleline comment */
{multiline}                 /* skip multiline comment */
\s+                         /* skip whitespace */
{dstring}                    yytext = yytext.substr(1, yyleng-2); return "STRING";
{sstring}                    yytext = yytext.substr(1, yyleng-2); return "STRING";
"{"                         return "{";
"}"                         return "}";
"("                         return "(";
")"                         return ")";
"["                         return "[";
"]"                         return "]";
";"                         return ";";
","                         return ",";
"sort"                      return "SORT";
"length"                    return "LENGTH";
"value"                     return "VALUE";
"updateValue"               return "UPDATE_VALUE";
"swap"                      return "SWAP";
"debug"                     return "DEBUG";
"var"                       return "VAR";
"if"                        return "IF";
"else"                      return "ELSE";
"while"                     return "WHILE";
"do"                        return "DO";
"for"                       return "FOR";
"break"                     return "BREAK";
"continue"                  return "CONTINUE";
"return"                    return "RETURN";
"true"                      return "TRUE";
"false"                     return "FALSE";
"null"                      return "NULL";
{integer}                   return "INTEGER";
{identifier}                return "IDENTIFIER";
"="                         return "=";
"*="                        return "*=";
"/="                        return "/=";
"%="                        return "%=";
"+="                        return "+=";
"-="                        return "-=";
">>="                       return ">>=";
"<<="                       return "<<=";
">>>="                      return ">>>=";
"&="                        return "&=";
"^="                        return "^=";
"|="                        return "|=";
"++"                        return "++";
"--"                        return "--";
">>>"                       return ">>>";
">>"                        return ">>";
"<<"                        return "<<";
"&&"                        return "&&";
"||"                        return "||";
"<="                        return "<=";
">="                        return ">=";
"=="                        return "==";
"!="                        return "!=";
"-"                         return "-";
"+"                         return "+";
"*"                         return "*";
"/"                         return "/";
"%"                         return "%";
"<"                         return "<";
">"                         return ">";
"^"                         return "^";
"|"                         return "|";
"&"                         return "&";
"!"                         return "!";
"~"                         return "~";
"?"                         return "?";
":"                         return ":";
<<EOF>>                     return "EOF";

/lex

%start test

%%

test
    : Program EOF
        {return $1;}
    ;

Program
    : /* empty */
    | Program Function
    ;

Function
    : SORT "(" ")" Block
    | IDENTIFIER "(" Parameters ")" Block
    ;

Parameters
    : /* empty */
    | IDENTIFIER
    | Parameters "," IDENTIFIER
    ;

Block
    : "{" BlockStatements "}"
    ;

BlockStatements
    : /* empty */
    | Statements
    ;

Statements
    : Statement
    | Statements Statement
    ;

ExpressionStatement
    : Expression
    ;

Statement
    : ";"
    | ExpressionStatement ";"
    | VariableDefinition ";"
    | Block
    | IF ParenthesizedExpression Block
    | IF ParenthesizedExpression Block ELSE Block
    | DO Block WHILE ParenthesizedExpression ";"
    | WHILE ParenthesizedExpression Block
    | FOR "(" ForInitializer ";" OptionalExpression ";" OptionalExpression ")" Block
    | CONTINUE ";"
    | BREAK ";"
    | RETURN OptionalExpression ";"
    ;

ForInitializer
    : /* empty */
    | Expression
    | VAR VariableDeclarationList
    ;

// Variables

VariableDefinition
    : VAR VariableDeclarationList
    ;

VariableDeclarationList
    : VariableDeclaration
    | VariableDeclarationList "," VariableDeclaration
    ;

VariableDeclaration
    : IDENTIFIER VariableInitializer
    ;

VariableInitializer
    : /* empty */
    | "=" AssignmentExpression
    ;

// Expressions

ParenthesizedExpression
    : "(" Expression ")"
    ;

Expression
    : AssignmentExpression
    | Expression "," AssignmentExpression
    ;

OptionalExpression
    : Expression
    | /* empty */
    ;

AssignmentExpression
    : ConditionalExpression
    | CallExpression AssignmentOperator AssignmentExpression
    ;

ConditionalExpression
    : BinaryExpression
    | BinaryExpression "?" AssignmentExpression ":" AssignmentExpression
    ;

BinaryExpression
    : UnaryExpression
    | BinaryExpression BinaryOperator UnaryExpression
    ;

UnaryExpression
    : PostfixExpression
    | DoubleUnaryOperator CallExpression
    | PrefixOperator UnaryExpression
    ;

PostfixExpression
    : CallExpression
    | CallExpression DoubleUnaryOperator
    ;

CallExpression
    : PrimaryExpression
    | CallExpression Arguments
    ;

Arguments
    : "(" ")"
    | "(" ArgumentList ")"
    ;

ArgumentList
    : AssignmentExpression
    | ArgumentList "," AssignmentExpression
    ;

PrimaryExpression
    : IDENTIFIER
    | INTEGER
    | LENGTH
    | VALUE
    | UPDATE_VALUE
    | SWAP
    | DEBUG
    | TRUE
    | FALSE
    | NULL
    | ParenthesizedExpression
    ;

AssignmentOperator
    : "="
    | "*="
    | "/="
    | "%="
    | "+="
    | "-="
    | ">>="
    | "<<="
    | ">>>="
    | "&="
    | "^="
    | "|="
    ;

BinaryOperator
    : "*"
    | "/"
    | "%"
    | "+"
    | "-"
    | "<<"
    | ">>"
    | ">>>"
    | "<"
    | ">"
    | "<="
    | ">="
    | "=="
    | "!="
    | "&"
    | "|"
    | "^"
    | "&&"
    | "||"
    ;

PrefixOperator
    : "!"
    | "~"
    | "-"
    ;

DoubleUnaryOperator
    : "--"
    | "++"
    ;
