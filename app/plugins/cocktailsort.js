//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// Sorting/app/plugins/cocktailsort.js
//
// Performs bubble sort on the provided data set.
//------------------------------------------------------------------------------

Sorting.cocktailsort = function() {

    this.name = 'Cocktail Sort';

    this.builtin = true;

    this.code = "sort() {\n\
    var begin = 0;\n\
    var end = length();\n\
    while (begin < end) {\n\
        newend = 0;\n\
        for (var i = 1; i < end; i++) {\n\
            if (value(i - 1) > value(i)) {\n\
                swap(i - 1, i);\n\
                newend = i;\n\
            }\n\
        }\n\
        end = newend;\n\
    \n\
        var newbegin = end;\n\
        for (var i = end - 1; i >= begin; i--) {\n\
            if (value(i + 1) < value(i)) {\n\
                swap(i + 1, i);\n\
                newbegin = i;\n\
            }\n\
        }\n\
        begin = newbegin;\n\
    }\n\
}\n\
";

    this.functions = {
        sort: {
            statements: [
                function (data) {
                    data.cs.begin = 0;
                },
                function (data) {
                    data.cs.end = data.length();
                },
                {
                    condition: function (data) {
                        return (data.cs.begin < data.cs.end);
                    },
                    statements: [
                        function (data) {
                            data.cs.newend = 0;
                        },
                        function (data) {
                            data.cs.i = 1;
                        },
                        {
                            condition: function (data) {
                                return (data.cs.i < data.cs.end);
                            },
                            statements: [
                                {
                                    condition: function (data) {
                                        return (data.value(data.cs.i - 1) > data.value(data.cs.i));
                                    },
                                    then: [
                                        function (data) {
                                            data.swap(data.cs.i - 1, data.cs.i);
                                        },
                                        function (data) {
                                            data.cs.newend = data.cs.i;
                                        }
                                    ]
                                },
                                function (data) {
                                    data.cs.i++;
                                }
                            ]
                        },
                        function (data) {
                            data.cs.end = data.cs.newend;
                        },
                        function (data) {
                            data.cs.newbegin = data.cs.end;
                        },
                        function (data) {
                            data.cs.i = data.cs.end - 1;
                        },
                        {
                            condition: function (data) {
                                return (data.cs.i >= data.cs.begin);
                            },
                            statements: [
                                {
                                    condition: function (data) {
                                        return (data.value(data.cs.i + 1) < data.value(data.cs.i));
                                    },
                                    then: [
                                        function (data) {
                                            data.swap(data.cs.i + 1, data.cs.i);
                                        },
                                        function (data) {
                                            data.cs.newbegin = data.cs.i;
                                        }
                                    ]
                                },
                                function (data) {
                                    data.cs.i--;
                                }
                            ]
                        },
                        function (data) {
                            data.cs.begin = data.cs.newbegin;
                        }
                    ]
                }
            ]
        }
    };
};
