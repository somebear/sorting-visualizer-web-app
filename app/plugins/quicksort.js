//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// Sorting/app/plugins/quicksort.js
//
// Performs quick sort on the provided data set.
//------------------------------------------------------------------------------

Sorting.quicksort = function() {

    this.name = 'Quick Sort';

    this.builtin = true;

    this.code = "sort() {\n\
    quicksort(0, length() - 1);\n\
}\n\
\n\
quicksort(p, r) {\n\
    if (p < r) {\n\
        q = partition(p, r);\n\
\n\
        quicksort(p, q);\n\
        quicksort(q + 1, r);\n\
    }\n\
}\n\
\n\
partition(p, r) {\n\
    x = value(p);\n\
    i = p - 1;\n\
    j = r + 1;\n\
\n\
    while (true) {\n\
        do {\n\
            j--;\n\
        } while (value(j) > x);\n\
        do {\n\
            i++;\n\
        } while (value(i) < x);\n\
\n\
        if (i < j) {\n\
            swap(i, j);\n\
        } else {\n\
            return j;\n\
        }\n\
    }\n\
}\n\
";

    this.functions = {
        sort: {
            statements: [
                {
                    invoke: 'quicksort',
                    arguments: function (data) {
                        return [0, data.length() - 1];
                    }
                }
            ]
        },
        quicksort: {
            arguments: ['p', 'r'],
            statements: [
                {
                    condition: function (data) {
                        return (data.cs.p < data.cs.r);
                    },
                    then: [
                        {
                            invoke: 'partition',
                            arguments: function (data) {
                                return [data.cs.p, data.cs.r];
                            }
                        },
                        function (data) {
                            data.cs.q = data.cs.retvalue;
                        },
                        {
                            invoke: 'quicksort',
                            arguments: function (data) {
                                return [data.cs.p, data.cs.q];
                            }
                        },
                        {
                            invoke: 'quicksort',
                            arguments: function (data) {
                                return [data.cs.q + 1, data.cs.r];
                            }
                        }
                    ]
                },
                {
                    return: function (data) {
                        return 0;
                    }
                }
            ]
        },
        partition: {
            arguments: ['p', 'r'],
            statements: [
                function (data) {
                    data.cs.x = data.value(data.cs.p);
                },
                function (data) {
                    data.cs.i = data.cs.p - 1;
                },
                function (data) {
                    data.cs.j = data.cs.r + 1;
                },
                {
                    condition: function (data) {
                        return true;
                    },
                    statements: [
                        function (data) {
                            data.cs.j--;
                        },
                        {
                            condition: function (data) {
                                return (data.value(data.cs.j) > data.cs.x);
                            },
                            statements: [
                                function (data) {
                                    data.cs.j--;
                                }
                            ]
                        },
                        function (data) {
                            data.cs.i++;
                        },
                        {
                            condition: function (data) {
                                return (data.value(data.cs.i) < data.cs.x);
                            },
                            statements: [
                                function (data) {
                                    data.cs.i++;
                                }
                            ]
                        },
                        {
                            condition: function (data) {
                                return (data.cs.i < data.cs.j);
                            },
                            then: [
                                function (data) {
                                    data.swap (data.cs.i, data.cs.j);
                                }
                            ],
                            else: [
                                {
                                    return: function (data) {
                                        return data.cs.j;
                                    }
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    };
};
