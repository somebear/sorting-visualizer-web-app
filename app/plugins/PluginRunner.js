//------------------------------------------------------------------------------
// Copyright 2013 Jonas Rabbe
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//------------------------------------------------------------------------------
// Sorting/app/plugins/PluginRunner.js
//
// Runs a plugin.
//
// A plugin is read from a file and turns into an object that can be understood
// by the program.
//
// The plugin object has the following signature:
//
// {
//     code: string
//
//     functions: [
//         {
//             condition: function (data) {...}
//             functions: [...]
//         }
//     |   function (data) {...}
//     ]
// }
//
// code
//      The basic human-readable version of the code that will be
//      shown in the plugin view.
//
// functions
//      Contains all the code that must be run. Each element can be either a
//      anonymous function with one argument (data), or an object with a
//      condition and a functions element. The condition element is a function
//      that takes the data object as an argument, and returns a boolean value.
//      The contents of the functions element will be run until the condition
//      element returns false.
//
//------------------------------------------------------------------------------

//
// Add clone() method to Array objects.
//
// Note: creates a copy that should be freed with 'delete'.
//

Array.prototype.clone = function () {
    return this.slice(0);
};

Array.prototype.last = function () {
    return this[this.length - 1];
}

//
// Sorting namespace.
//

var Sorting = {};

//
// PluginRunner class.
//

self.pluginRunner = {

    //
    // Variables.
    //

    _values: [],
    _timeout: 0,
    _plugin: '',
    _stop: false,

    //
    // Methods.
    //

    setValues: function (values) {
        this._values = values;
    },

    setTimeout: function (timeout) {
        this._timeout = timeout;
    },

    load: function (plugin) {
        this._plugin = plugin;
        importScripts(this._plugin + '.js');
        this._pluginData = new Sorting[this._plugin];
    },

    run: function () {
        this._stop = false;
        var functions = this._pluginData.functions;
        var statements = functions.sort.statements.clone();
        var stack = [];
        var data = {
            length: function () {
                return this._values.length;
            }.bind(this),
            value: function (idx) {
                return this._values[idx];
            }.bind(this),
            updateValue: function (idx, value) {
                this.updateValue(idx, value);
            }.bind(this),
            swap: function (idx1, idx2) {
                this.swap(idx1, idx2);
            }.bind(this),

            debug: function (msg) {
                this.debug(msg);
            }.bind(this),

            cs: {},  // current scope
            ps: [],  // previous scope(s)
            sd: [0]  // stack depth (for current function)
        };

        delete pluginData;

        if (statements.length)
            this._runner(functions, statements, stack, data);
    },

    stop: function () {
        this._stop = true;
    },

    //
    // Callbacks.
    //

    updateValue: function (idx, value) {
        this._values[idx] = value;
        postMessage({ cmd: 'updateValue', idx: idx, value: value });
    },

    swap: function (idx1, idx2) {
        var tmp = this._values[idx1];
        this._values[idx1] = this._values[idx2];
        this._values[idx2] = tmp;
        postMessage({ cmd: 'swap', idx1: idx1, idx2: idx2 });
    },

    done: function () {
        postMessage({ cmd: 'done' });
    },

    debug: function (msg) {
        postMessage({ cmd: 'log', message: "PluginRunner: " + msg});
    },

    //
    // Internal functionality.
    //

    _runner: function (functions, statements, stack, data) {
        var next = statements.shift();

        if (typeof next == 'function') {
            next(data);
        } else if (typeof next == 'object') {
            if('invoke' in next && 'arguments' in next) {
                if (next['invoke'] in functions) {
                    data.sd.push(1);
                    data.ps.push(data.cs);
                    stack.push(statements);

                    var f = functions[next['invoke']];

                    var a = {};
                    var na = next.arguments(data);
                    for (var i = 0; i < na.length; i++) {
                        if (i >= na.length || i >= f.arguments.length)
                            break;

                        a[f.arguments[i]] = na[i];
                    }
                    data.cs = a;

                    statements = f.statements.clone();
                }
            } else if ('return' in next) {
                var r = next.return(data);
                data.cs = data.ps.pop();
                data.cs.retvalue = r;

                var sd = data.sd.pop();
                for (var i = 0; i < sd; i++) {
                    statements = stack.pop();
                }
            } else if('condition' in next) {
                if (next.condition(data)) {
                    // TODO implement proper support for do ... while loops.
                    if ('statements' in next) {
                        if (statements.length > 0) {
                            data.sd[data.sd.length - 1]++;
                            stack.push(statements);
                        }

                        data.sd[data.sd.length - 1]++;
                        stack.push([next]);

                        // NOTE we clone the next functions because we might
                        // need them again for the next iteration.
                        statements = next.statements.clone();
                    } else if ('then' in next) {
                        data.sd[data.sd.length - 1]++;
                        stack.push(statements);
                        statements = next.then.clone();
                    }
                } else {
                    if ('else' in next) {
                        data.sd[data.sd.length - 1]++;
                        stack.push(statements);
                        statements = next.else.clone();
                    }
                }
            }
        }

        if (statements.length == 0) {
            if (stack.length == 0) {
                // If we have no more functions, neither in f nor l, then we
                // are done.
                delete data;
                this.done();
                return;
            }

            statements = stack.pop();
            data.sd[data.sd.length - 1]--;
        }

        if (this._stop)
            return;

        setTimeout(
            function () {
                this._runner(functions, statements, stack, data);
            }.bind(this),
            this._timeout);
    }
};

//
// Message handler.
//

self.addEventListener('message', function (e) {
    var data = e.data;

    switch (data.cmd) {
        case 'setValues':
            self.pluginRunner.setValues(data.values);
            break;
        case 'setTimeout':
            self.pluginRunner.setTimeout(data.timeout);
            break;
        case 'load':
            self.pluginRunner.load(data.plugin);
            break;
        case 'start':
            self.pluginRunner.run();
            break;
        case 'stop':
            self.pluginRunner.stop();
            break;
    };
}, false);
